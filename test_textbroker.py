import unittest
from hashlib import md5

import textbroker

class TestTextBroker(unittest.TestCase):

    def setUp(self):
        # Setup the client
        self.textbroker_client = textbroker.TextBrokerClient(
            'd2ca91e9621adcf2275c68aa205706f5', 'abc')

    def test_get_token(self):
        password = self.textbroker_client.password
        salt, token = self.textbroker_client.get_token()
        # Test if the token hash is ok
        self.assertEqual(
            md5((salt + password).encode('utf-8')).hexdigest(), token)

    def test_do_login(self):
        # Test the login method
        self.assertEqual(self.textbroker_client.do_login(), True)

        # Test if the exception is being raised if the login fails
        self.textbroker_client.password = (self.textbroker_client.password 
                                           + 'cba')
        with self.assertRaises(textbroker.TextBrokerLoginError):
            self.textbroker_client.do_login()

    def test_get_client_balance(self):
        try:
            self.textbroker_client.get_client_balance()
        except Exception:
            self.fail('Method getClientBalance raised an unexpected error.')

    def test_get_name(self):
        try:
            self.textbroker_client.get_name()
        except Exception:
            self.fail('Method getName raised an unexpected error.')

    def test_get_usage(self):
        try:
            self.textbroker_client.get_usage()
        except Exception:
            self.fail('Method getUsage raised an unexpected error.')

    def test_get_is_in_sandbox(self):
        try:
            self.textbroker_client.get_is_in_sandbox()
        except Exception:
            self.fail('Method isInSandbox raised an unexpected error.')

    def test_get_actual_period_data(self):
        try:
            self.textbroker_client.get_actual_period_data()
        except Exception:
            self.fail('Method getActualPeriodData raised an unexpected error.')
    


if __name__ == '__main__':
    unittest.main()
        
