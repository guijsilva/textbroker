import random
import logging
from hashlib import md5

from suds import Client

logger = logging.getLogger('textbroker')

class TextBrokerClient:
    """
    The base class to interact with the textbroker api
    """
    def __init__(self, budget_key, password):
        # Define the budgetKey and password attributes
        self.budget_key = budget_key
        self.password = password

    def get_token(self):
        '''
        Method to return the hashed token with it's respective salt
        '''
        # The salt is a random value in each call, toe respect the api 
        # recomendations
        salt = str(random.randrange(1, 10000))
        token = md5((salt + self.password).encode('utf-8')).hexdigest()

        return salt, token

    def do_login(self):
        '''
        Call the api doLogin method
        '''

        wsdl = 'https://api.textbroker.com/Budget/loginService.php?wsdl'
        location = 'https://api.textbroker.com/Budget/loginService.php'
        salt, token = self.get_token()
        client = Client(wsdl, location=location)

        response = client.service.doLogin(salt, token, self.budget_key)

        # Raise an exception if the login fails
        if not response:
            logger.exception('doLogin method returned false')
            raise TextBrokerLoginError
        else:
            return response

    def get_client_balance(self):
        '''
        Call the api getClientBalance method
        '''
        wsdl = 'https://api.textbroker.com/Budget/budgetCheckService.php?wsdl'
        location = 'https://api.textbroker.com/Budget/budgetCheckService.php'
        salt, token = self.get_token()
        client = Client(wsdl, location=location)

        response = client.service.getClientBalance(
            salt, token, self.budget_key)

        return response

    def get_name(self):
        '''
        Call the api getName method
        '''
        wsdl = 'https://api.textbroker.com/Budget/budgetCheckService.php?wsdl'
        location = 'https://api.textbroker.com/Budget/budgetCheckService.php'
        salt, token = self.get_token()
        client = Client(wsdl, location=location)

        response = client.service.getName(salt, token, self.budget_key)

        return response

    def get_usage(self):
        '''
        Call the api getUsage method
        '''
        wsdl = 'https://api.textbroker.com/Budget/budgetCheckService.php?wsdl'
        location = 'https://api.textbroker.com/Budget/budgetCheckService.php'
        salt, token = self.get_token()
        client = Client(wsdl, location=location)

        response = client.service.getUsage(
            salt, token, self.budget_key)

        return response

    def get_is_in_sandbox(self):
        '''
        Call the api isInSandbox method
        '''
        wsdl = 'https://api.textbroker.com/Budget/budgetCheckService.php?wsdl'
        location = 'https://api.textbroker.com/Budget/budgetCheckService.php'
        salt, token = self.get_token()
        client = Client(wsdl, location=location)

        response = client.service.isInSandbox(
            salt, token, self.budget_key)

        return response

    def get_actual_period_data(self):
        '''
        Call the api getActualPeriodData method
        '''
        wsdl = 'https://api.textbroker.com/Budget/budgetCheckService.php?wsdl'
        location = 'https://api.textbroker.com/Budget/budgetCheckService.php'
        salt, token = self.get_token()
        client = Client(wsdl, location=location)

        response = client.service.getActualPeriodData(
            salt, token, self.budget_key)

        return response


# ========
# Exceptions
# ========

class TextBrokerLoginError(Exception):
    pass
